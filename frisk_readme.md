# Frisk Theme

## How to install
The installation process is pretty easy!
Simply download the .zip above and throw it into the root folder of your webspace.
If you already have a kirby starterkit running, delete the `assets`, `content` and `site` folders and replace them with the folders from this theme-package.

If you want to change the main color of this theme, simply go to the site-settings in your panel.

The theme comes with the Features-page from this demo, which shows you how to use all the different elements in Markdown and Kirbytext. Make sure to have a look at how to use these cool buttons, or the columns. ;)

## License
This theme is **free to use** with your Kirby installation. However I would appreciate it if you acknowledge my work in the footer of your website. Therefor it is licensed under a (link: http://creativecommons.org/licenses/by/4.0/ text: Creative Commons Attribution 4.0 International License).

Please note, that you’ll still have to (link: http://getkirby.com/buy text: buy a License for Kirby).

## Acknowledgements
Frisk theme uses the code of two plugins to realise certain features:
- The realisation of (link: http://getkirby.com/blog/columns-in-kirbytext text: Columns by Bastian Allgeier)
- (link: https://github.com/ian-cox/Kirby-Color-Picker text: The kirby color picker by ian-cox) to give you the ability to change the color scheme with just a few clicks

## Any questions left?
Don’t hesitate to contact me at kirby@kiwipalme.de